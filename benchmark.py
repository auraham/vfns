# benchmark.py
# Run this script using ipython as follows:
#
# %run benchmark.py
# %timeit run_benchmark(1)
# %timeit run_benchmark(2) 
# %timeit run_benchmark(4) 

from __future__ import print_function
import numpy as np
from vfns import vfns
from vfns_seq import vfns_seq
from fnds import fnds_debug, fnds
from plot import plot_time    


def run_benchmark(procs):
    
    # load objs
    objs_a = np.genfromtxt("data/weights_m_3_a.txt")
    objs_b = np.genfromtxt("data/weights_m_3_b.txt")
    objs_c = np.genfromtxt("data/weights_m_3_c.txt")
    objs_d = np.genfromtxt("data/weights_m_3_d.txt")
    
    pops = [objs_a, objs_b, objs_c, objs_d]
    lbls = ["a", "b", "c", "d"]
    
    objs = np.vstack(pops)
    
    # sort objs concurrently
    fronts, ranks = vfns(objs, procs)
    
    # add first front
    front = objs[fronts[0]]
    pops.append(front.copy())
    lbls.append("First front")
    
    # plot
    #plot_pops(pops, lbls)
    
if __name__ == "__main__":
    pass
    #run_benchmark(procs=4)
