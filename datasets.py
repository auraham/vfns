# create_nds_datasets.py
from __future__ import print_function
from reference_vectors import create_reference_vectors
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from numpy.random import RandomState

if __name__ == "__main__":
    
    params = (
        # (m_objs, H1, H2)
        (3, 6, 7),
        (5, 5, 6),
        (8, 5, 6),
        (10, 3, 7),
        )
        
    n_datasets = 2
    
    for m_objs, H1, H2 in params:
        
        for seed in range(n_datasets):
            
            rand = RandomState(seed)
            
            # create layers
            first_layer  = create_reference_vectors(m_objs, H1) 
            second_layer = create_reference_vectors(m_objs, H2) * 100
            
            # merge layers
            merge = np.vstack((first_layer, second_layer))
            
            # indices
            n_points = merge.shape[0]
            indices = np.arange(n_points, dtype=int)
            ranks = np.ones((n_points, ), dtype=int)
            
            # set ranks
            a_points = first_layer.shape[0]
            ranks[:a_points] = 0
            
            # shuffle?
            if seed > 0:
                
                rand.shuffle(indices)
                ranks = ranks[indices]
                merge = merge[indices]
                
            #else:
                # no shuffle
                
            # save points
            filename = "input/dataset_m_%d_h1_%d_h2_%d_n_%d.txt" % (m_objs, H1, H2, seed)
            np.savetxt(filename, merge)
            
            # save indices
            filename = "input/dataset_m_%d_h1_%d_h2_%d_n_%d_indices.txt" % (m_objs, H1, H2, seed)
            np.savetxt(filename, indices, fmt="%d")
            
            # save ranks
            filename = "input/dataset_m_%d_h1_%d_h2_%d_n_%d_ranks.txt" % (m_objs, H1, H2, seed)
            np.savetxt(filename, ranks, fmt="%d")
            
            print(filename)
            
            
    # test
    objs = np.genfromtxt("input/dataset_m_3_h1_6_h2_7_n_0.txt")
    ranks = np.genfromtxt("input/dataset_m_3_h1_6_h2_7_n_0_ranks.txt")
    
    # first front
    to_keep = ranks == 0
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")

    ax.plot(objs[:, 0], objs[:, 1], objs[:, 2], "bo", label="second front")
    ax.plot(objs[to_keep, 0], objs[to_keep, 1], objs[to_keep, 2], "go", label="first front")



    ax.legend(loc="upper right")
    ax.view_init(30, 45)

    plt.show()
