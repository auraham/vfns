# dominance.py
from __future__ import print_function
import numpy as np

A_DOM_B         =  1
A_IS_DOM_BY_B   = -1
A_EQUALS_TO_B   =  0
NON_DOMINATED   = -2

def compare_min(a, b):
    """
    Pareto dominance for minimization
    
    Input
    a               (m_objs, ) array
    b               (m_objs, ) array

    Output
    flag_dominance  int value: 1, -1, 0, -2
    """
    
    m_objs = a.shape[0]
    
    flag_diff       = False     # we assume that a and b are the same vector
    flag_dominance  = None      # result    
    flag_aux        = None      # auxiliar flag
    
    
    # this loop allow us to determine whether a and b
    # are the same objective vector, and give an initial
    # value for flag_aux 
    for i in range(m_objs):
        
        if (a[i] == b[i]):              # same i-th objective value, it has no sense to compare
            continue
        
        elif (a[i] < b[i]):
            flag_aux  = A_DOM_B
            flag_diff = True            # the i-th objective value of both a and b is different
            break                       # ie, a and b are different objective vectors
        
        else:
            flag_aux  = A_IS_DOM_BY_B
            flag_diff = True
            break
    
    
    # if both objective vectors are the same
    # return this flag
    if not flag_diff:
        return A_EQUALS_TO_B
        
        
    # otherwise, determine which of them is better 
    for i in range(m_objs):
        
        if (a[i] == b[i]):
            continue                        # same i-th objective value, it has no sense to compare
            
        if (a[i] < b[i]):
            flag_dominance = A_DOM_B
        else:
            flag_dominance = A_IS_DOM_BY_B
            
        
        if (flag_aux != flag_dominance):        
            return NON_DOMINATED            # there is a change in domination, thus a and b are non-dominated
            
        
    return flag_dominance


def compare_max(a, b):
    """
    Pareto dominance for maximization
    
    Input
    a               (m_objs, ) array
    b               (m_objs, ) array

    Output
    flag_dominance  int value: 1, -1, 0, -2
    """
    
    m_objs = a.shape[0]
    
    flag_diff       = False     # we assume that a and b are the same vector
    flag_dominance  = None      # result    
    flag_aux        = None      # auxiliar flag
    
    
    # this loop allow us to determine whether a and b
    # are the same objective vector, and give an initial
    # value for flag_aux 
    for i in range(m_objs):
        
        if (a[i] == b[i]):              # same i-th objective value, it has no sense to compare
            continue
        
        elif (a[i] > b[i]):
            flag_aux  = A_DOM_B
            flag_diff = True            # the i-th objective value of both a and b is different
            break                       # ie, a and b are different objective vectors
        
        else:
            flag_aux  = A_IS_DOM_BY_B
            flag_diff = True
            break
    
    
    # if both objective vectors are the same
    # return this flag
    if not flag_diff:
        return A_EQUALS_TO_B
        
        
    # otherwise, determine which of them is better 
    for i in range(m_objs):
        
        if (a[i] == b[i]):
            continue                        # same i-th objective value, it has no sense to compare
            
        if (a[i] > b[i]):
            flag_dominance = A_DOM_B
        else:
            flag_dominance = A_IS_DOM_BY_B
            
        
        if (flag_aux != flag_dominance):        
            return NON_DOMINATED            # there is a change in domination, thus a and b are non-dominated
            
        
    return flag_dominance


if __name__ == "__main__":
    
    flags = {
        A_DOM_B: "A_DOM_B", 
        A_IS_DOM_BY_B: "A_IS_DOM_BY_B",
        A_EQUALS_TO_B: "A_EQUALS_TO_B",
        NON_DOMINATED: "NON_DOMINATED",
    }
    
    # minimization
    # (a, b)
    print("\nminimization")
    pairs = (
        (np.array([0.1, 0.2, 0.1]),  np.array([0.1, 0.2, 0.3])),        #  1: A_DOM_B
        (np.array([0.2, 0.2, 0.3]),  np.array([0.1, 0.2, 0.3])),        # -1: A_IS_DOM_BY_B
        (np.array([0.1, 0.2, 0.3]),  np.array([0.1, 0.2, 0.3])),        #  0: A_EQUALS_TO_B
        (np.array([0.0, 0.2, 0.3]),  np.array([0.1, 0.2, 0.2])),        # -2: NON_DOMINATED
        )

    for a, b in pairs:
        
        result = compare_min(a, b)
        print("result", flags[result])
    
    
    
    # maximization
    # (a, b)
    print("\nmaximization")
    pairs = (
        (np.array([0.2, 0.2, 0.3]),  np.array([0.1, 0.2, 0.3])),        #  1: A_DOM_B
        (np.array([0.1, 0.1, 0.3]),  np.array([0.1, 0.2, 0.3])),        # -1: A_IS_DOM_BY_B
        (np.array([0.1, 0.2, 0.3]),  np.array([0.1, 0.2, 0.3])),        #  0: A_EQUALS_TO_B
        (np.array([0.1, 0.2, 0.4]),  np.array([0.2, 0.2, 0.3])),        # -2: NON_DOMINATED
        )

    for a, b in pairs:
        
        result = compare_max(a, b)
        print("result", flags[result])
    
