# fnds.py
# Fast non-dominated sorting
from __future__ import print_function
from dominance import compare_min, A_DOM_B, A_IS_DOM_BY_B
import numpy


def fnds_debug(objs, compare=compare_min):
    """
    Fast Non-Dominated Sorting (debug)
    
    Input
    objs        (pop_size, m_objs) objs matrix
    
    Output
    sp          ...
    np          ...
    """
    
    n = objs.shape[0]
    
    sp      = [ []   for i in range(n) ]
    np      = [ 0    for i in range(n) ]
    ranks   = [10000 for i in range(n) ]
    
    fronts  = [[]]
    
    for p in range(n):
        
        for q in range(n):
            
            a = objs[p, :]
            b = objs[q, :]
    
            result = compare(a, b) 

            if result == A_DOM_B:
                # add q-th solution dominated by p-th solution
                sp[p].append(q)
            
            elif result == A_IS_DOM_BY_B:
                # increment domination counter of p-th solution
                np[p] += 1
                
        if np[p] == 0:
            ranks[p] = 0
            fronts[0].append(p)
    
    return tuple(sp), tuple(np)


def fnds(objs, compare=compare_min):
    """
    Fast Non-Dominated Sorting
    
    Input
    objs        (pop_size, m_objs) objs matrix
    
    Output
    fronts      tuple of tuples of indices
    ranks       (pop_size, ) int array
    """
    
    n = objs.shape[0]
    
    sp      = [ []   for i in range(n) ]
    np      = [ 0    for i in range(n) ]
    ranks   = [10000 for i in range(n) ]
    
    fronts  = [[]]
    
    for p in range(n):
        
        for q in range(n):
            
            a = objs[p, :]
            b = objs[q, :]
    
            result = compare(a, b) 

            if result == A_DOM_B:
                # add q-th solution dominated by p-th solution
                sp[p].append(q)
            
            elif result == A_IS_DOM_BY_B:
                # increment domination counter of p-th solution
                np[p] += 1
                
        if np[p] == 0:
            ranks[p] = 0
            fronts[0].append(p)
            
    
    i = 0
    while len(fronts[i]) > 0:
        
        Q = []
        
        for p in fronts[i]:
            
            for q in sp[p]:
                
                np[q] -= 1
                
                if np[q] == 0:
                    ranks[q] = i+1
                    Q.append(q)
                    
        i += 1
        
        if len(Q) > 0:
            fronts.append(list(Q))
        else:
            break
    
    
    fronts_tuple = tuple( [list(front) for front in fronts ] )
    
    return fronts_tuple, numpy.array(ranks, dtype=int)
    

if __name__ == "__main__":
    
    import matplotlib.pyplot as plt
    
    fig = plt.figure("nds")
    ax = fig.add_subplot(111)
    ax.cla()
    ax.set_xlabel("$f_1$")
    ax.set_ylabel("$f_2$")
    
    objs = numpy.array([
        [0.31, 6.10],
        [0.43, 6.79],
        [0.22, 7.09],
        [0.59, 7.85],
        [0.66, 3.65],
        [0.83, 4.23],
        [0.21, 5.90],
        [0.79, 3.97],
        [0.51, 6.51],
        [0.27, 6.93],
        [0.58, 4.52],
        [0.24, 8.54]])
        
    ax.plot(objs[:, 0], objs[:, 1], "bo")
    
    for ind, p in enumerate(objs):
        x, y = p
        ax.text(x, y, "%s" % ind)
        
    plt.show()
    
    fronts, ranks = fnds(objs)
    
        

