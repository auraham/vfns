# main.py
from __future__ import print_function
import numpy as np
from vfns import vfns
from vfns_seq import vfns_seq
from fnds import fnds_debug, fnds
from plot import plot_pop

if __name__ == "__main__":
    
    objs = np.array([
        [5.9, 3.8],     # 1
        [4.8, 9.0],     # 2 
        [1.0, 2.0],     # 3
        [7.0, 8.5],     # 4
        [2.2, 6.1],     # 5
        [9.0, 3.0],     # 6
        [2.8, 1.8],     # 7
        [3.0, 9.6],     # 8
    ])

    # sort objs concurrently
    fronts, ranks = vfns(objs, procs=4)
    
    # output
    print("fronts      ", fronts)
    print("ranks       ", ranks)

    # sort objs sequentially (for reference)
    fronts_ref, ranks_ref = vfns_seq(objs)

    # compare outputs
    print("fronts (ref)", fronts_ref)
    print("ranks  (ref)", ranks_ref)
    
    # plot
    plot_pop(objs, fronts, output="fronts.png")

    # get all the solutions of the 2-th front (zero-based index)
    front_2 = objs[ranks == 2]
    front_2 = objs[fronts[2]]
