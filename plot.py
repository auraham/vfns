# plot.py
# Plotting utilities
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.pyplot import rcParams

colors = [
        "#74CE74",
        "#FFA500",
        "#F7523A",
        "#5A5A5A",
        "#0759AB",
        "y",
        "m",
        "g",
        "c",
        "#F9FF81"]

basecolors = {
    "real_front"    : "#7C8577",
    "z_ideal"       : "#ECF037",
    "z_nadir"       : "#F03785",
    "to_keep"       : "#8CDC6B",
    "to_replace"    : "#620F0E",
    "almost_black"  : "#262626",
    "weights"       : "#E4AFC8",
    "weights_hl"    : "#7D2950",
    "cherry_dark"   : "#A91458",
    "cherry_light"  : "#E4AFC8",
    "jitter"        : "#74CE74",
    "jitter_dark"   : "#539453",
}

def load_rcparams(figsize=None):
    """
    Load a custom rcParams dictionary
    """
    
    rcParams['axes.titlesize']  = 14            # title
    rcParams['axes.labelsize']  = 16            # $f_i$ labels
    rcParams['xtick.color']     = "#474747"     # ticks gray color
    rcParams['ytick.color']     = "#474747"     # ticks gray color
    rcParams['xtick.labelsize'] = 10            # ticks size
    rcParams['ytick.labelsize'] = 10            # ticks size
    rcParams['legend.fontsize'] = 12            # legend
    rcParams['legend.fontsize'] = 12            # legend
                
    if isinstance(figsize, tuple):
        rcParams['figure.figsize'] = figsize    # figure size


def draw_lines(ax, objs, fronts):
    """
    Draw lines connecting the points of a front
    """
    
    for front in fronts:
        
        if len(front) > 1:
        
            a = objs[front, :]         # set of objs
            x_vals = a[:, 0]         
            
            sorted_ids = x_vals.argsort()
            
            sorted_objs = a[sorted_ids, :]
            
            n_points = len(front)
            for i in range(n_points-1):
                
                A = sorted_objs[i]          # (m_objs, )
                B = sorted_objs[i+1]        # (m_objs, )
                
                C = np.array([[A[0], A[1]],
                            [B[0], B[1]]])
                
                ax.plot(C[:, 0], C[:, 1], "#808080")


def plot_pop(objs, fronts, output="", lower_lims=(0,0,0), upper_lims=(12,12,12)):
    """
    Plot a solution set sorted in fronts
    """
    
    # check
    pop_size, m_objs = objs.shape
    if m_objs > 3:
        print("plot_pops.py, m_objs: %s > 3, it is not possible to plot" % m_objs)
        return None

    if m_objs == 2:
        load_rcparams((10, 4))
    else:
        load_rcparams((10, 8))
    
    # create plot
    fig = plt.figure()
    ax  = fig.add_subplot(111) if m_objs == 2 else fig.add_subplot(111, projection="3d")

    # set labels
    ax.set_xlabel("$f_1$", rotation=0)
    ax.set_ylabel("$f_2$", rotation=0)
    if m_objs == 3:
        ax.xaxis.set_rotate_label(False)
        ax.yaxis.set_rotate_label(False)
        ax.zaxis.set_rotate_label(False)
        ax.set_zlabel("$f_3$", rotation=0)
    
    # set lims
    if not (lower_lims is None or upper_lims is None):
    
        ax.set_xlim(lower_lims[0], upper_lims[0])
        ax.set_ylim(lower_lims[1], upper_lims[1])
        
        if m_objs == 3:
            ax.set_zlim(lower_lims[2], upper_lims[2])

    # change panes
    if m_objs == 3:
        ax.w_xaxis.set_pane_color((0.95, 0.95, 0.95, 1.0))
        ax.w_yaxis.set_pane_color((0.95, 0.95, 0.95, 1.0))
        ax.w_zaxis.set_pane_color((0.95, 0.95, 0.95, 1.0))
    
    if m_objs == 2:
        
        draw_lines(ax, objs, fronts)
        
        ax.plot(objs[:, 0], objs[:, 1], c="#A91458", 
                mec="#262626", ls="none", marker="o", markeredgewidth=0.15)
                
        for i, v in enumerate(objs):
            x, y = v
            ax.text(x+0.1, y+0.1, str(i))
            
        
        # adjust margins (smaller height)
        fig.subplots_adjust(left=0.12,         # seems ok
                            bottom=0.15,        # seems ok
                            right=0.95,        # ok
                            top=0.9,           # ok
                            wspace=0.38,
                            hspace=0.68,
                            )
            
    elif m_objs == 3:
        
        ax.plot(objs[:, 0], objs[:, 1], objs[:, 2], c="#A91458", 
                mec="#262626", ls="none", marker="o", markeredgewidth=0.15)

        ax.view_init(30, 45)

    if output:
        print(output)
        fig.savefig(output, dpi=300)
    
    plt.show()


def plot_pops(pops, labels, title=None, figname=None, real_front=None, p=None, plot_surface=False, 
        lower_lims=None, upper_lims=None, output="", show_plot=True, z_ideal=None,
        z_nadir=None, loc="lower left", ncol=2, show_legend=True, custom_colors=None, custom_ms=None, elev=30, azim=45):
    """
    Plot a set of points in 2D and 3D.
    
    pops            list of l point matrices of size (n_points, m_objs)
    labels          list of l string labels, one for each point matrix
    
    title           string, plot title
    figname         string, figure name
    real_front      matrix, Pareto front
    p               int, spacing parameter needed for ploting real_front using a wireframe
    plot_surface    bool, determines if the wireframe will be filled
    lower_lims      (m_objs, ) array, lower bounds of the plot
    upper_lims      (m_objs, ) array, upper bounds of the plot
    output          string, output path
    show_plot       bool, determines if the plot will be shown (useful when the plot will be saved but not shown)
    z_ideal         (m_objs, ) array, z ideal vector
    z_nadir         (m_objs, ) array, z ideal nadir
    loc             string, legend location
    ncol            int, number of legend columns
    show_legend     bool, enable legend
    custom_colors   tuple, list with colors for pops
    """
    
    # check
    m_objs = pops[0].shape[1]
    if m_objs > 3:
        print("plot_pops.py, m_objs: %s > 3, it is not possible to plot" % m_objs)    # @todo: add log support here
        # return fig, ax
        return None, None

    if m_objs == 2:
        load_rcparams((10, 4))
    else:
        load_rcparams((10, 8))
    
    # create plot
    fig = plt.figure(figname) if figname else plt.figure()
    ax  = fig.add_subplot(111) if m_objs == 2 else fig.add_subplot(111, projection="3d")

    # set labels
    ax.set_xlabel("$f_1$", rotation=0)
    ax.set_ylabel("$f_2$", rotation=0)
    if m_objs == 3:
        ax.xaxis.set_rotate_label(False)
        ax.yaxis.set_rotate_label(False)
        ax.zaxis.set_rotate_label(False)
        ax.set_zlabel("$f_3$", rotation=0)

    # set title
    if title:
        ax.set_title(title)

    # set lims
    if not (lower_lims is None or upper_lims is None):
    
        ax.set_xlim(lower_lims[0], upper_lims[0])
        ax.set_ylim(lower_lims[1], upper_lims[1])
        
        if m_objs == 3:
            ax.set_zlim(lower_lims[2], upper_lims[2])
    
    # change panes
    if m_objs == 3:
        ax.w_xaxis.set_pane_color((0.95, 0.95, 0.95, 1.0))
        ax.w_yaxis.set_pane_color((0.95, 0.95, 0.95, 1.0))
        ax.w_zaxis.set_pane_color((0.95, 0.95, 0.95, 1.0))
    
            
    # change the axis title to almostblack
    ax.title.set_color(basecolors["almost_black"])
    
    # callback
    lined = {}      # lines dictionary
    lines = []      # lines list
    
    # custom colors
    if custom_colors is None:
        custom_colors = tuple(colors)       # @todo: maybe tuple() is not needed
    
    # custom marker sizes
    if custom_ms is None:
        custom_ms = tuple([6 for i in range(len(pops))])
    
    # pops
    for i, pop in enumerate(pops):
        
        if m_objs == 2:

            l, = ax.plot(pop[:, 0], pop[:, 1], label=labels[i], c=custom_colors[i], 
                        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15, ms=custom_ms[i])
            
            lines.append(l)
            lined[labels[i]] = l
            
        elif m_objs == 3:
            l, = ax.plot(pop[:, 0], pop[:, 1], pop[:, 2], label=labels[i], c=custom_colors[i], 
                        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15, ms=custom_ms[i])
            lines.append(l)
            lined[labels[i]] = l
    
    # legend
    if show_legend:
        
        leg = ax.legend(loc=loc, scatterpoints=1, ncol=ncol, numpoints=1)
        
        # for callback
        for legtxt, __ in zip(leg.get_texts(), lines):
            legtxt.set_picker(5)
        
    # inner callback
    def on_pick(event):
        
        legtxt, line = None, None
        
        obj = event.artist
        is_callable = callable(getattr(obj, "get_text", None))
        
        if is_callable:
            
            legtxt = event.artist
            line = lined[legtxt.get_text()]
            
        else:
            
            legtxt = None
            line = event.artist
            
        is_visible = line.get_visible()
        
        # fade a little bit
        if is_visible:
            
            line.set_visible(False)
            
            if legtxt is not None:
                legtxt.set_alpha(0.2)
                
        else:
            
            line.set_visible(True)
            
            if legtxt is not None:
                legtxt.set_alpha(None)
                
        fig.canvas.draw()
    
    if m_objs == 3:
        ax.view_init(elev, azim)
    
    # register callback only if legend is visible
    if show_legend:
        #fig.canvas.callbacks.connect("pick_event", on_pick)
        fig.canvas.mpl_connect("pick_event", on_pick)

    # save before plt.show
    if output:                                                  # @todo: check if output is a valid path
        #fig.set_size_inches(width, height)
        fig.savefig(output, dpi=300, bbox_inches="tight")
    
    plt.show()

 
def plot_time(time_list, time_lbls, output=""):
    
    load_rcparams((10, 4))
    rcParams['axes.labelsize'] = 12            # $f_i$ labels

    # create plot
    fig = plt.figure()
    ax  = fig.add_subplot(111)
    
    # prepare timeseries
    n = len(time_list)
    x = np.arange(n)
    y_mean = np.zeros((n, ))
    y_std = np.zeros((n, ))
    
    for i, (times, lbl) in enumerate(zip(time_list, time_lbls)):

        # mean and std of times
        time_mean = np.mean(times)
        time_std = np.std(times)
        
        y_mean[i] = time_mean
        y_std[i] = time_std
        
    # plot series
    ax.plot(x, y_mean, c="#A91458", 
            mec="#262626", marker="o", markeredgewidth=0.15)
            
    # set labels
    ax.set_xlabel("Procs", rotation=0)
    ax.set_ylabel("Time (ms)")
    
    # set limits
    ax.set_xlim(-0.25, (n-1)+0.25)
    
    # setup xticks
    ax.set_xticks([ i for i in range(n)])
    ax.set_xticklabels([time_lbls[i] for i in range(n)], rotation=0) 

    # adjust margins (smaller height)
    fig.subplots_adjust(left=0.12,         # seems ok
                        bottom=0.15,        # seems ok
                        right=0.95,        # ok
                        top=0.9,           # ok
                        wspace=0.38,
                        hspace=0.68,
                        )
                        
                        
    
    if output:
        print(output)
        fig.savefig(output, dpi=300)
    
    plt.show()

