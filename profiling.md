# Profiling

**tl;dr** This post describes the design of `vfns.py`. Before trying to parallelize each `for` loop, we conduct a profiling test to find the bottleneck of the sequential implementation, `vfns_seq.py`. Then, we discuss how to tackle them using `concurrent.futures`.



## Introduction

**Problem** We have a sequential implementation of VFNS in `vfns_seq.py`. Now, we want a parallel/concurrent version.

Before attempting to parallelize every `for` loop or matrix-based operation, we must consider the bottlenecks of the current sequential algorithm. By bottleneck, we mean those portions of code that requires the most part of the computational time. Also, the time required for these bottleneck portions are proportional to the input size (`pop_size` in our case). If we can identify such regions,  and they can be divided, we can reduce their computational time by using parallelism. 

Other reason to identify these bottlenecks instead of parallelize every `for` loop is because this approach could not be efficient. ...

To this end, we will use 





```
python3 profiling.py
```

This is the output

```

```







delete from here

```
  
*** PROFILER RESULTS ***
run_profile (profiling.py:8)
function called 1 times

         137774 function calls (137346 primitive calls) in 0.358 seconds

   Ordered by: cumulative time, internal time, call count
   List reduced from 508 to 40 due to restriction <40>

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.000    0.000    0.358    0.358 profiling.py:8(run_profile)
        1    0.000    0.000    0.329    0.329 vfns_seq.py:40(vfns_seq)
        1    0.057    0.057    0.328    0.328 vfns_seq.py:9(compute_dominance_matrix)
   112560    0.271    0.000    0.271    0.000 dominance.py:11(compare_min)

```

