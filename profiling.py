# profiling.py
# python3 profiling.py
from __future__ import print_function
import numpy as np
from vfns_seq_profiling import vfns_seq_profiling
from profilehooks import profile

@profile
def run_profile():
    
    # load objs
    objs = np.genfromtxt("input/dataset_m_5_h1_5_h2_6_n_0.txt")
    objs = np.genfromtxt("input/dataset_m_10_h1_3_h2_7_n_0.txt")
    objs = np.genfromtxt("input/dataset_m_8_h1_5_h2_6_n_0.txt")

    # sort objs sequentially
    fronts_a, ranks_a = vfns_seq_profiling(objs)


if __name__ == "__main__":
    
    run_profile()
    
