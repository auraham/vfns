# Very Fast Non-dominated Sorting 

This repository contains a Python implementation of VFSN [[Smutnicki14]](http://dominik.zelazny.staff.iiar.pwr.wroc.pl/pliki/dorobek/czasopisma/2014%20-%20DMMS%20vol.%208%20issue%201-2%20-%20Rudy%20Jaros%C5%82aw,%20Smutnicki%20Czes%C5%82aw,%20%C5%BBelazny%20Dominik.pdf). This method is a parallel version of the Fast Non-dominated Sorting (FNDS) [[Deb02]](https://www.iitk.ac.in/kangal/Deb_NSGA-II.pdf). Unlike VFSN, FNDS is a sequential method. Both methods are employed with the aim of sorting a set of solutions (`m`-dimensional vectors) into non-dominated fronts. The computational time for this sorting operation depends on the number of solutions and the dimension (also called number of objectives, `m`).  This repository provides a python implementation of both methods. It is organized as follows:

| File           | Description                                                  |
| -------------- | ------------------------------------------------------------ |
| `main.py`      | Example.                                                     |
| `vfns.py`      | Concurrent implementation of VFNS based on `concurrent.futures`. |
| `vfns_seq.py`  | Sequential implementation of VFNS, just for getting a better insight into the algorithm. |
| `dominance.py` | Implementation of Pareto dominance.                          |
| `benchmark.py` | This script compares the speedup of `vfns.py` for `n=2, 4, 8, 16` processes. |
| `vfns.md`      | This file contains details of the algorithm.                 |
| `fnds.py`      | Fast non-dominated sorting (FNDS) [Deb02].                   |
| `plot.py`      | Plotting utilities.                                          |



# Demo

Run `main.py` as follows:

```
python3 main.py
```

This script sorts a solution set into fronts as shown in the next figure:

![](fronts.png)



`vfns` returns two variables:

- `fronts` is a list of fronts, where the `i`-th item in `fronts` contains the solutions (indices) of the `i`-th front.

- `ranks` is a numpy array with `n` elements (i.e., `ranks`), where `ranks[i]` is the rank of the `i`-th solution. From [Deb02], all solutions are ranked according to their non-domination level based on the objective function values.

In this example, we have this output:

```
fronts       ((2, 6), (0, 4, 5), (1, 3, 7))
ranks        [1 2 0 2 1 1 0 2]
```

that is, there are three fronts: `(2, 6)`, `(0, 4, 5)`, and `(1, 3, 7)`. These fronts are shown in the above figure. In addition, each solution has a rank shown in `ranks`. In this case, `ranks[0] = 1` means that `objs[0]`, i.e., the first solution, belongs to the front `1`. Alternatively, we can get all the solutions in the `i`-th front using `ranks` as follows:


```python
# get all the solutions of the 2-th front (zero-based index)
front_2 = objs[ranks == 2]
front_2 = objs[fronts[2]]
```



# How to use it

You can use the `vfns` function to sort a solution set `objs` as follows:

```python
# main.py
from vfns import vfns

# sort a set of vectors using 4 processes
fronts, ranks = vfns(objs, procs=4)
```

Here, `objs` is a numpy array of shape `(pop_size, m_objs)`.



# Benchmark

In this section, we test the execution time of `vfns` regarding `procs=1, 2, 4` processes. First, create the datasets as follows:

```python
In [1]: cd data/                      
In [2]: %run create_datasets.py
```

Next, evaluate the [execution time](https://jakevdp.github.io/PythonDataScienceHandbook/01.07-timing-and-profiling.html) using `%timeit` in `ipython`.

```python
%run benchmark.py
%timeit run_benchmark(1)      # procs = 1
%timeit run_benchmark(2)      # procs = 2
%timeit run_benchmark(4)      # procs = 4 
```

This is the output in my laptop:

```
In [2]: %timeit run_benchmark(1)                         
533 ms ± 1.87 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)

In [3]: %timeit run_benchmark(2)                         
284 ms ± 6.22 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)

In [4]: %timeit run_benchmark(4)                         
281 ms ± 862 µs per loop (mean ± std. dev. of 7 runs, 1 loop each)
```

The execution time is reduced in the second case using `procs = 2`. Although, the reduction is marginal in the last case.

Alternatively, you can use this command to save the outcome of `%timeit` into a variable:

```
In [8]: res = %timeit -o run_benchmark(4)                 
283 ms ± 796 µs per loop (mean ± std. dev. of 7 runs, 1 loop each)

In [9]: res                                            
Out[9]: <TimeitResult : 283 ms ± 796 µs per loop (mean ± std. dev. of 7 runs, 1 loop each)>
```

Finally, you can plot the mean execution time as follows:

```
%run benchmark.py
res_1 = %timeit -o run_benchmark(1)                         
res_2 = %timeit -o run_benchmark(2)                         
res_4 = %timeit -o run_benchmark(4)                         

time_list = (res_1.all_runs, res_2.all_runs, res_4.all_runs)
time_lbls = ("1", "2", "4")

plot_time(time_list, time_lbls, output="time.png")
```

![](time.png)




# TODO

- rename `par_vfns` to `vfns`
- return `fronts`, `ranks` in `vfns(objs, procs=12)`
- terminar la version sequencial para devolver los frentes y ranks
- tomar esa version como referencia para la version concurrente
- describir el benchmarking
- hacer un script, o indicar cómo hacer un profiling, para mostrar que el bottleneck (region con mayor computo) del algoritmo es el calculo de la matrix
- un script `plot.py` para graficar las poblaciones en 2D y 3D (algunos datasets3)
- usar linting



# References

- [Smutnicki14] **Very Fast Non-Dominated Sorting**. *Decision Making in Manufacturing and Services*.
- [Deb02] **A Fast and Elitist Multiobjective Genetic Algorithm: NSGA-II**. *IEEE Transactions on Evolutionary Computation.*
- [VanderPlas16] **Python Data Science Handbook** [online](https://jakevdp.github.io/PythonDataScienceHandbook/01.07-timing-and-profiling.html)


