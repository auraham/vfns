# Very Fast Non-dominated Sorting

**tl;dr** *This algorithm is a parallel implementation of the fast non-dominated sorting proposed by Deb et al. This post provides a gentle introduction to this algorithm, a benchmarking analysis, and code using Python + futures.*

## Introduction

**Problem** Given a set of $N$ $m$-dimensional vectors, we want to:

- Sort them according to Pareto dominance, that is, arrange them in fronts.
- Filter them, that is, retrieve the first front.

In this post, such a set of vectors is called *population*. Consider the population show in Fig. (1), with $N=8$ vectors arranged in three fronts:
$$
F_1 = \{3, 7\}, F_2 = \{5,1, 6\}, F_3 = \{8, 2, 4\}
$$


![](fronts.png)

**Figure 1** *Population and fronts.*



 **Sequential algorithm** One of the most popular approaches for sorting a set of vectors according to Pareto dominance is the fast non-dominated sorting proposed by Deb et al. [Deb02]. Its time complexity is $O(N^{2}m)$. As the number of vectors $N$ or the dimension $m$ grows, the computational time increases considerably, limiting the scope of this algorithm. This algorithm is employed as a intermediate step for selecting a subset of points. For instance, considering the NSGA-II and NSGA-III, given a population of $2N$, the vectors are first sorted by using fast-non dominated sorting to identify the best individuals according to Pareto dominance.

This algorithm has other applications, such as:

- Survival selection for multiobjective evolutionary algorithms.
- Preprocessing for computing performance indicators.

This algorithm is employed for some multiobjective evolutionary algorithms to assist in the survival selection step. The general procedure is as follows. Given a parent and offspring population, $P_t$ and $P'_t$, we want to identify the best $N$ individuals from $P_t \cup P'_t$ according to Pareto dominance. First, they are arranged in fronts, $F_1, F_2, \dots, F_l$. Then, we keep the first $k$ fronts as follows:
$$
P_{t+1} = P_{t+1} \cup F_i, \mbox{ while } |P_{t+1} \cup F_i| < N, \mbox{ for } i = 1, \dots, l
$$
where $P_{t+1}$denotes the next population for generation $t+1$. The remaining $N- |P_{t+1}|$ slots are filled using a density estimator, such as crowding distance [Deb02].

This algorithm can also be employed to filter a population before computing a performance indicator, such as the Hypervolume [Zitzler99]. This indicator measures the extension of the non-dominated region enveloped by a set of $m$-dimensional vectors. Fig [] gives a better intuition of this indicator. In practical settings, only the first front of $P$ is required for calculating its Hypervolume. Thus, the fast non-dominated sorting can be used to *filter* the population, returning the first front as a result.

[hypervolume figure]

Surely there are more applications, but these ones are two of the most frequently ...

The main drawback of this algorithm is its time complexity. Many attempts to reduce it have been made in the literature [...]. This post describes one of them, called Very *Fast Non-dominated Sorting*, or VFNS for short, proposed by Smutnicki et al. [Smutnicki14]. Unlike other approaches [...], VFNS is a parallel algorithm suitable for CUDA or concurrent languages, such as Go.  As a proof-of-concept, a Python implementation is provided with this post. First, a sequential implementation of VFNS is described. Later, a concurrent implementation is developed by using the `concurrent.futures` package. The rest of the post is advocated to benchmark the Fast non-dominated sorting, sequential VFNS, and concurrent VFNS implementations. The code is available [[here]](https://bitbucket.org/auraham_cg/vfns).

## Algorithm

This section provides a gentle introduction to VFNS. To ease discussion and implementation, we will use the following structures:

**Dominance matrix** $D \in \mathbb{N}^{N \times N}$


$$
D[i, j] = \begin{cases}
                1 & \mbox{ if } \mathbf{x}_i \prec \mathbf{x}_j \\
                0 & \mbox{ otherwise }
                \end{cases}
$$

where $D[i, j]$ denotes the Pareto dominance relation between two solutions, $\mathbf{x}_i$ and $\mathbf{x}_j$, for $i=1, \dots, N$, and $j=1, \dots, N$.

**Hits vector** $\mathbf{d} = [d_1, \dots, d_N]^T$


$$
d_j = \sum_i D[i, j], \mbox{ for } j = 1, \dots, N
$$
In other words, $d_j$ is the sum of the $j$-th column of $D$.  Given $d_i = j$, $j$ is the number of solutions that dominates (are better than) $\mathbf{x}_i$. In this post, $T$ denotes transpose. 



**Mask** $\mathbf{m} = [m_1, \dots, m_N]^T$
$$
m_i = \begin{cases}
                1 & \mbox{ if } \mathbf{x}_i \mbox{ is available} \\
                0 & \mbox{ otherwise }
                \end{cases}
$$
if $m_i=1$, then $\mathbf{x}_i$ can be added to a given front. Otherwise, $\mathbf{x}_i$ is unavailable, that is, it belongs to a given front.

**Ranks** $\mathbf{r} = [r_1, \dots, r_N]$

where $r_i = j$ means that solution $\mathbf{x}_i$ belongs to front $F_j$. The lower the rank, the better the quality of the solution according to Pareto dominance.



[put pseudocode here]



These variables are summarized as follows:

| Variable                 | Code    | Shape                  | Type   |
| ------------------------ | ------- | ---------------------- | ------ |
| Dominance matrix $D$     | `D`     | `(pop_size, pop_size)` | `int`  |
| Hits vector $\mathbf{d}$ | `hits`  | `(pop_size, )`         | `int`  |
| Mask $\mathbf{m}$        | `mask`  | `(pop_size, )`         | `bool` |
| Ranks $\mathbf{r}$       | `ranks` | `(pop_size, )`         | `int`  |
|                          |         |                        |        |



## Example

Consider the following population depicted in Fig. (1):

```
objs = np.array([
        [5.9, 3.8],     # 1
        [4.8, 9.0],     # 2 
        [1.0, 2.0],     # 3
        [7.0, 8.5],     # 4
        [2.2, 6.1],     # 5
        [9.0, 3.0],     # 6
        [2.8, 1.8],     # 7
        [3.0, 9.6],     # 8
    ])
```

First, the dominance matrix `D` is computed:

```python
# D
array([[0, 0, 0, 1, 0, 0, 0, 0],
       [0, 0, 0, 0, 0, 0, 0, 0],
       [1, 1, 0, 1, 1, 1, 0, 1],
       [0, 0, 0, 0, 0, 0, 0, 0],
       [0, 1, 0, 1, 0, 0, 0, 1],
       [0, 0, 0, 0, 0, 0, 0, 0],
       [1, 1, 0, 1, 0, 1, 0, 1],
       [0, 0, 0, 0, 0, 0, 0, 0]])
```

**First front**

From this matrix, compute `hits`:

```python
# hits
array([2, 3, 0, 4, 1, 2, 0, 3])
```

This vector determines the number of solutions that dominates (are better than) a given vector $\mathbf{x}_i$. For instance:

```python
hits[2] = 0
hits[6] = 0
```

mean that solutions $\mathbf{x}_2$ and $\mathbf{x}_6$ are not dominated by any solutions, while

```python
hits[1] = 3
```

implies that $\mathbf{x}_3$ is dominated by (inferior than) three solutions: $\mathbf{x}_2$, $\mathbf{x}_4$, and $\mathbf{x}_6$. An easy way to know which solutions dominate $\mathbf{x}_3$ is to see the positions $D[:, 3]$ equal to 1, that is, $D[2,3], D[4, 3],$ and $D[6,3]$. 

Then, we use `hits` and `mask` to determine which solutions are added to the next front. We employ the following rule:

```python
to_keep = (hits == 0) & (mask == 1)
```

This is a shortcut for

```python
if hits[i] == 0 and mask[i] == 1:
	to_keep[i] = True
else:
	to_keep[i] = False
```

From our example:

```python
hits    [    2,     3,     0,     4,     1,     2,     0,     3]
mask    [ True,  True,  True,  True,  True,  True,  True,  True]
                          |                           |
                          |                           |
to_keep [False, False,  True, False, False, False,  True, False]
```

The intuition behind this rule is as follows: If $\mathbf{x}_i$ is 

- available for selection

```python
mask[i] == True
```

- not dominated by any solution

```python
hits[i] == 0
```

then, mark $\mathbf{x}_i$ to be added in the next front:

```python
to_keep[i] = True
```

We know that $\mathbf{x}_2$ and $\mathbf{x}_6$ are marked for selection. They are added to the next front as follows:

```python
new_front = indices[to_keep]
fronts.append(tuple(new_front))        
```

where:

```python
indices   [0, 1, 2, 3, 4, 5, 6, 7]
to_keep   [F, F, T, F, F, F, T, F]
new_front [2, 6]
```

Next, we update `ranks`:

```python
# update ranks
ranks[to_keep] = r
r += 1
```

In this example:

```
ranks     [-1, -1, -1, -1, -1, -1, -1, -1]
to_keep   [ F,  F,  T,  F,  F,  F,  T,  F]

# after update: ranks[to_keep] = r
ranks     [-1, -1,  0, -1, -1, -1,  0, -1]
```

In other words,  $\mathbf{x}_2$ and $\mathbf{x}_6$ have the same rank, `r=0`, because they belong to the same front $F_r$.

Then, we mark both solutions as *unavailable* to prevent add them in subsequent iterations:

```python
mask[to_keep] = 0
```

That is:

```python
mask      [T, T, T, T, T, T, T, T]
to_keep   [F, F, T, F, F, F, T, F]

# after update: mask[to_keep] = 0
mask     [T, T, F, T, T, T, F, T]
```

Later, update `D` as follows:

```
D[to_keep, :] = 0
```

What is the intuition of this? Well, consider `D` and `hits` before update:

```python
# D
array([[0, 0, 0, 1, 0, 0, 0, 0],
       [0, 0, 0, 0, 0, 0, 0, 0],
       [1, 1, 0, 1, 1, 1, 0, 1],   # x_2
       [0, 0, 0, 0, 0, 0, 0, 0],
       [0, 1, 0, 1, 0, 0, 0, 1],
       [0, 0, 0, 0, 0, 0, 0, 0],
       [1, 1, 0, 1, 0, 1, 0, 1],   # x_6
       [0, 0, 0, 0, 0, 0, 0, 0]])

# hits
       [2, 3, 0, 4, 1, 2, 0, 3]
```

`hits[i]=j` tells me that there are `j` solutions better than solution `i`. However, as some solutions were marked ($\mathbf{x}_2$, $\mathbf{x}_6$), we need to update `hits`, and therefore, update `D`. 



**Second front**





**Final front**



Now, let's talk about profiling.



## Profiling

The aim of profiling `vfns_seq.py` is to determine which sections of code take most of the execution time. Once these sections have been identified, they could be parallelized to reduce the execution time.



## Concurrent implementation 

According to our profiling, the creation of `D` takes a considerable amount of time. In this section, we explain how to create the dominance matrix `D` concurrently.

I have used the terms *parallel* and *concurrent* interchangeably. However, they are slightly different. We can divide a task into $n$ smaller tasks. If we have $n$ computation units (cores), then we can say that we are using a parallel approach to solve our problem. However, if we have a single computation unit to process $n$ tasks, we are using a concurrent approach to solve the problem. If that was not clear, check this image from [[Joe Armstrong's blog]](https://joearms.github.io/published/2013-04-05-concurrent-and-parallel-programming.html).



[Concurrent and parallel programming]

![](con_and_par.jpg)



 vfns_seq.py`, c



**todo**

- continua colocando el ejemplo para ilustrar el second front, third front
- ​
- agregar seccion *Sequencial Implementation*, *Concurrent Implementation*, *Benchmarking*, *Conclusions*


## Footnotes

[1] For instance, the paper use the term *domination matrix $D$*, whereas we employed *dominance matrix $R$*. Also, the construction of this matrix is slightly different. In the paper, the authors say:

> [...] we create a matrix of size $N \times N$ called the *domination matrix* $D$. The element $d_{i, j}$ of this matrix indicates whether a solution $i$ dominates [is better than] solution $j$.

In this post, the element $R[i, j]$ (or $r_{i, j}$ following the notation of Smutnicki et al.) indicates whether a solution $i$ is dominated by [is worser than] solution $j$.

We think that this change ease implementation because, later in the paper, the authors say:

> The assignment works as follows: for each solution we check whether its previously computed sum equals 0.

By that, the authors means the sum by row of $R$:

```
ranks = R.sum(axis=0)
```

If `ranks[i]=0`, then solution $i$ is non-dominated and belongs to the first front. Computing `ranks`allows us to filter/construct the first front. However, that filtering is not possible if matrix $D$ was computed instead. For this reason, we change the use of $D$ by $R$.



## References

- [Deb02] **A Fast and Elitist Multiobjective Genetic Algorithm: NSGA-III**. *IEEE Transactions on Evolutionary Computation, 6(2):182-197.*
- [Zitzler99] **Multiobjective Evolutionary Algorithms: A Comparative Case Study and the Strength Pareto Approach**. *IEEE Transactions on Evolutionary Computation, 3(4):257-271.*
- [Smutnicki14] **Very Fast Non-Dominated Sorting**. *Decision Making in Manufacturing and Services*.