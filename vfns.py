# vfns.py
from __future__ import print_function
import numpy as np
import os, sys, argparse
from dominance import compare_min, A_DOM_B
from concurrent.futures import ProcessPoolExecutor, as_completed


def get_block(pid, rows, procs):
    """
    Return a tuple (start, end) which defines the batch of rows for a given process.
    
    Input
    pid             int, process id
    rows            int, number of rows (ie, population size)
    procs           int, number of processes (workers)
    
    Output
    (start, end)    tuple, batch
    """
    
    bs = int(np.ceil(rows/procs))       # block size
    
    start = (pid)*bs
    end = (pid+1)*bs
    
    if end > rows:
        #print("reset: end from %d to %d" % (end, rows))
        end = rows
    
    # more explicit, more memory
    #block = np.arange(start, end, dtype=int)
    
    # less memory
    block = (start, end)
    
    return block


def compute_dominance_matrix_block(pid, objs, block):
#def vfns_block(pid, objs, block):
    """
    Compute a part of dominance matrix D defined by block
    
    Input
    objs            (pop_size, m_objs) objs matrix
    block           (start, end) int tuple, define the rows to be processed by the pid process
    
    Output
    D_block         (rows, pop_size) int matrix, part of dominance matrix D
    """
    
    # debug
    #print("pid: %d, block: (%d, %d)" % (pid, block[0], block[1])) 
    
    start, end = block
    indices = np.arange(start, end, dtype=int)
    
    pop_size, m_objs = objs.shape
    rows = len(indices)
    
    D_block = np.zeros((rows, pop_size), dtype=int)
    
    for row, i in enumerate(indices):
        for j in range(pop_size):
            
            if i == j:
                D_block[row, j] = 0
                continue
            
            a = objs[i]     # x_i
            b = objs[j]     # x_j
            
            result = compare_min(a, b)
            
            if result == A_DOM_B:
                
                # x_i is better than x_j
                D_block[row, j] = 1
                
                # @note: it is  R[row, j], not R[i, j] because i > rows
    
    return pid, D_block.copy(), block


def par_compute_dominance_matrix(objs, procs):
    """
    Compute dominance matrix D using a set of procs workers (processes).
    To this end, a batch of rows of D is assigned to each worker.
    
    Input
    objs            (pop_size, m_objs) matrix
    procs           int, number of processes
    
    Output
    D               (pop_size, pop_size) dominance matrix
    """
    
    pop_size, m_objs = objs.shape
    
    # return
    D = np.zeros((pop_size, pop_size), dtype=int)           # dominance matrix
    
    # -- parallel region starts --
    
    futures = []
    
    with ProcessPoolExecutor() as manager:
        
        # submit futures
        for pid in range(procs):
            
            block = get_block(pid, pop_size, procs)         # (start, end) batch
        
            task = manager.submit(compute_dominance_matrix_block, pid, objs, block)
            futures.append(task)
            
        # wait for futures
        for f in as_completed(futures):
            
            # get partial result
            pid, D_block, block = f.result()
            
            # update dominance matrix
            start, end = block
            D[start:end, :] = D_block.copy()
    
    # -- parallel region ends --
    
    return D.copy()


def vfns(objs, procs=12):
    """
    Parallel (concurrent) implementation of VFNS
    Return a list of fronts and their ranks
    
    Input
    objs            (pop_size, m_objs) matrix
    procs           int, number of processes
    
    Output
    fronts_tuple    tuple of list, each subtuple is a front
    ranks           (pop_size, ) int array, where ranks[i]=j: the i-th solution has the rank j
                    if j is -1, objs[i] has not been ranked/assigned into a front
    """
    
    pop_size, m_objs = objs.shape
    
    # return
    fronts = []
    ranks = np.ones((pop_size, ), dtype=int) * -1
    
    mask = np.ones((pop_size, ), dtype=bool)
    indices = np.arange(pop_size, dtype=int)
    by_col = 0
    r = 0
    
    # compute dominance matrix (concurrently)
    D = par_compute_dominance_matrix(objs, procs)
    
    while mask.sum() > 0:
        
        # compute hits
        hits = D.sum(axis=by_col)
        
        # select solutions for next front
        to_keep = (hits == 0) & (mask == 1)
        new_front = indices[to_keep]
        fronts.append(tuple(new_front))
        
        # update ranks
        ranks[to_keep] = r
        r += 1 
        
        # update mask and domination matrix
        mask[to_keep] = 0
        D[to_keep, :] = 0
        
    # save fronts as a tuple of lists
    fronts_tuple = tuple([ list(front) for front in fronts ])
    
    return fronts_tuple, ranks.copy()


if __name__ == "__main__":
    
    """
    pid: 0, block: (0, 3), indices: [0 1 2]
    pid: 1, block: (3, 6), indices: [3 4 5]
    pid: 2, block: (6, 9), indices: [6 7 8]
    reset: end from 12 to 10
    pid: 3, block: (9, 10), indices: [9]
    """

    rows = 10
    procs = 4
    
    for pid in range(procs):
        
        block = get_block(pid, rows, procs)
        indices = np.arange(block[0], block[1], dtype=int)
        
        print("pid: %d, block: %s, indices: %s" % (pid, block, indices))
    
    
    
