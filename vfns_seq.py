# vfns_seq.py
from __future__ import print_function
import numpy as np
import os, sys, argparse
from dominance import compare_min, A_DOM_B


def compute_dominance_matrix(objs):
    """
    Compute dominance matrix D, where
        D[i, j] = 1 means that solution i is better than (dominates) j
    """
    
    pop_size, m_objs = objs.shape

    D = np.zeros((pop_size, pop_size), dtype=int)
    
    for i in range(pop_size):
        
        for j in range(pop_size):
            
            if i == j:
                continue
            
            a = objs[i]     # x_i
            b = objs[j]     # x_j
            
            result = compare_min(a, b)
            
            if result == A_DOM_B:
                
                # x_i is better than x_j
                D[i, j] = 1
                
    return D.copy()


def vfns_seq(objs):
    """
    Sequential implementation of VFNS
    Return a list of fronts and their ranks
    
    Input
    objs            (pop_size, m_objs) matrix
    
    Output
    fronts_tuple    tuple of tuples, each subtuple is a front
    ranks           (pop_size, ) int array, where ranks[i]=j: the i-th solution has the rank j
                    if j is -1, objs[i] has not been ranked/assigned into a front
    """
    
    pop_size, m_objs = objs.shape
    
    # return
    fronts = []
    ranks = np.ones((pop_size,), dtype=int) * -1
    
    mask = np.ones((pop_size, ), dtype=bool)
    indices = np.arange(pop_size, dtype=int)
    by_col = 0
    r = 0
    
    # compute dominance matrix
    D = compute_dominance_matrix(objs)
    
    while mask.sum() > 0:
        
        # compute hits
        hits = D.sum(axis=by_col)
        
        # select solutions for next front
        to_keep = (hits == 0) & (mask == 1)
        new_front = indices[to_keep]
        fronts.append(tuple(new_front))
        
        # update ranks
        ranks[to_keep] = r
        r += 1 
        
        # update mask and domination matrix
        mask[to_keep] = 0
        D[to_keep, :] = 0
        
    
    # save fronts as tuples
    fronts_tuple = tuple([ tuple(front) for front in fronts ])
    
    return fronts_tuple, ranks.copy()
